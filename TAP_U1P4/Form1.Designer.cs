﻿namespace TAP_U1P4
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNombre = new System.Windows.Forms.Label();
            this.textBoxCliente = new System.Windows.Forms.TextBox();
            this.labelBoletos = new System.Windows.Forms.Label();
            this.numericUpDowBoletos = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxAsientos = new System.Windows.Forms.ComboBox();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxSeleccion = new System.Windows.Forms.ListBox();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.buttonFinalizar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowBoletos)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNombre
            // 
            this.labelNombre.AccessibleName = "labelCliente";
            this.labelNombre.AutoSize = true;
            this.labelNombre.Location = new System.Drawing.Point(37, 52);
            this.labelNombre.Name = "labelNombre";
            this.labelNombre.Size = new System.Drawing.Size(98, 13);
            this.labelNombre.TabIndex = 0;
            this.labelNombre.Text = "Nombre del cliente:";
            // 
            // textBoxCliente
            // 
            this.textBoxCliente.AccessibleName = "";
            this.textBoxCliente.Location = new System.Drawing.Point(165, 52);
            this.textBoxCliente.Name = "textBoxCliente";
            this.textBoxCliente.Size = new System.Drawing.Size(311, 20);
            this.textBoxCliente.TabIndex = 1;
            // 
            // labelBoletos
            // 
            this.labelBoletos.AccessibleName = "";
            this.labelBoletos.AutoSize = true;
            this.labelBoletos.Location = new System.Drawing.Point(37, 104);
            this.labelBoletos.Name = "labelBoletos";
            this.labelBoletos.Size = new System.Drawing.Size(99, 13);
            this.labelBoletos.TabIndex = 2;
            this.labelBoletos.Text = "Número de boletos:";
            this.labelBoletos.Click += new System.EventHandler(this.label1_Click);
            // 
            // numericUpDowBoletos
            // 
            this.numericUpDowBoletos.Location = new System.Drawing.Point(165, 104);
            this.numericUpDowBoletos.Name = "numericUpDowBoletos";
            this.numericUpDowBoletos.Size = new System.Drawing.Size(196, 20);
            this.numericUpDowBoletos.TabIndex = 3;
            this.numericUpDowBoletos.ValueChanged += new System.EventHandler(this.numericUpDowBoletos_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Selecciona tus asientos:";
            // 
            // comboBoxAsientos
            // 
            this.comboBoxAsientos.FormattingEnabled = true;
            this.comboBoxAsientos.Location = new System.Drawing.Point(165, 152);
            this.comboBoxAsientos.Name = "comboBoxAsientos";
            this.comboBoxAsientos.Size = new System.Drawing.Size(196, 21);
            this.comboBoxAsientos.TabIndex = 5;
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(401, 155);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(75, 23);
            this.buttonGuardar.TabIndex = 6;
            this.buttonGuardar.Text = "Guardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Selección:";
            // 
            // listBoxSeleccion
            // 
            this.listBoxSeleccion.FormattingEnabled = true;
            this.listBoxSeleccion.Location = new System.Drawing.Point(40, 238);
            this.listBoxSeleccion.Name = "listBoxSeleccion";
            this.listBoxSeleccion.Size = new System.Drawing.Size(436, 134);
            this.listBoxSeleccion.TabIndex = 8;
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonEliminar.Location = new System.Drawing.Point(40, 390);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(75, 23);
            this.buttonEliminar.TabIndex = 9;
            this.buttonEliminar.Text = "Eliminar";
            this.buttonEliminar.UseVisualStyleBackColor = false;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // buttonFinalizar
            // 
            this.buttonFinalizar.BackColor = System.Drawing.Color.Teal;
            this.buttonFinalizar.Location = new System.Drawing.Point(401, 390);
            this.buttonFinalizar.Name = "buttonFinalizar";
            this.buttonFinalizar.Size = new System.Drawing.Size(75, 23);
            this.buttonFinalizar.TabIndex = 10;
            this.buttonFinalizar.Text = "Finalizar";
            this.buttonFinalizar.UseVisualStyleBackColor = false;
            this.buttonFinalizar.Click += new System.EventHandler(this.buttonFinalizar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 450);
            this.Controls.Add(this.buttonFinalizar);
            this.Controls.Add(this.buttonEliminar);
            this.Controls.Add(this.listBoxSeleccion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.comboBoxAsientos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDowBoletos);
            this.Controls.Add(this.labelBoletos);
            this.Controls.Add(this.textBoxCliente);
            this.Controls.Add(this.labelNombre);
            this.Name = "Form1";
            this.Text = "Venta Boletos";
            this.Load += new System.EventHandler(this.label1_Click);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowBoletos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNombre;
        private System.Windows.Forms.TextBox textBoxCliente;
        private System.Windows.Forms.Label labelBoletos;
        private System.Windows.Forms.NumericUpDown numericUpDowBoletos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxAsientos;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBoxSeleccion;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.Button buttonFinalizar;
    }
}

